xquery version "3.1";
(: disable xinclude in xml parser options :)

(:~ 
    This script is to merge splitted
    source documents in a single xml
    file. It extends the text nodes by
    an additional element to maintain
    the order of text nodes in so
    called mixed-content nodes.
    
    @version 0.0.1
    @author Mathias Göbel
    @copyright SUB Göttingen
:)

declare copy-namespaces no-preserve, inherit;

declare namespace output = "http://www.w3.org/2010/xslt-xquery-serialization";
declare namespace tgmd="http://textgrid.info/namespaces/metadata/core/2010";
declare namespace tei="http://www.tei-c.org/ns/1.0";
declare namespace xi="http://www.w3.org/2001/XInclude";
declare default element namespace "http://www.tei-c.org/ns/1.0";

declare option output:method "xml";
declare option output:indent "false";
declare option output:omit-xml-declaration "true";

declare %private function local:resolve-include($textgrid_uri as xs:string)
(:as element(tei:div):)
{
    doc('export/' || replace($textgrid_uri, ':', '_') || '.xml')
};

(:~
   Identity Template with XInclude resolution.
:)
declare %private function local:apply-templates($nodes as node()*)
as node()* {
for $node in $nodes
return
    typeswitch ($node)
        case element(xi:include) return
                local:resolve-include( $node/@href => string() )
                => local:apply-templates()
        case element() return 
            if ($node eq $node/root())
            then
                element {  $node/node-name() } {
                    local:apply-templates($node/(@*|node()))
                }
            else
            element { $node/node-name() } {
                local:apply-templates($node/(@*|node()))
            }
        case attribute() return $node
        case text() return 
            if
              (
                matches($node, '\S')
                and $node/parent::tei:*/local-name() = ('p', 'q', 'head') 
                and not($node/ancestor::tei:teiHeader)
              )
            then
                element { "seg" } { $node }
            else $node
    default return local:apply-templates($node/(@*|node()))
};

(:~
  creates an absolute xpath for any given node
:)
declare function local:make-xpath($node as node()) as xs:string {
'/' || (: starting slash for absolute path :)
(for $i in $node/ancestor-or-self::*
    return
        $i/local-name() ||
            (if ( not(exists( $i/parent::*/*[name()=name($i)][2])) ) then '' else
        '%5B' || count($i/preceding-sibling::*[name()=name($i)]) + 1 || '%5D')
    ) => string-join('/')
};

declare %private function local:add-xpath($nodes as node()*)
as node()* {
for $node in $nodes
return
    typeswitch ($node)
        case element(tei:seg) return
                element { $node/node-name() } {
                    attribute { 'xml:id' } { generate-id($node) },
                    attribute {'sameAs'} { local:make-xpath($node) },
                    $node/node()
                }
        case element() return
                element { $node/node-name() } {
                    local:add-xpath($node/(@*, node()))
                }
        case text() return $node
        default return $node
};

(: local context item as input (Saxon: -s argument) :)
let $doc := .

return
    local:apply-templates( $doc )
    => local:add-xpath()
