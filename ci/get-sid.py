"""
    getting a SID for TextGrid
"""

import os
import requests

PY_PROJECT_PATH = os.environ['CI_PROJECT_PATH'].replace('/', '-')
BASE_URL = 'http://vault:8000/'
BASE_PARAMS = {
    'role_name': f'gitlab-{PY_PROJECT_PATH}',
    'path': 'rosenzweig/textgrid',
}
SID_SERVICE_URL="https://sid.ahiqar.sub.uni-goettingen.de/"

USERNAME = requests.get(f'{BASE_URL}?key=user', params=BASE_PARAMS, timeout=10).text
print(USERNAME)
PASSWORD = requests.get(f'{BASE_URL}?key=pass', params=BASE_PARAMS, timeout=10).text
SID = requests.post(SID_SERVICE_URL, json={"username": USERNAME, "password": PASSWORD}, timeout=10).json()

# Writing to file
with open(".env", "w", encoding='UTF-8') as env_file:
    # Writing data to a file
    env_file.write(f'export SID={SID}')
