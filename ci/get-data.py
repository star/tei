"""
    getting all files from TextGrid
"""

import os
from tgclients import TextgridCrud
from lxml import etree

ENCODING = 'UTF-8'
SID = os.environ['SID']
crud = TextgridCrud()

base_file_str = crud.read_data('textgrid:40zz7', sid=SID).text
os.makedirs('export', exist_ok=True)
with open(os.path.join('export', 'base.xml'), 'w', encoding=ENCODING) as file:
    file.write(base_file_str)

tree = etree.fromstring(bytes(base_file_str, ENCODING))
includes = tree.findall('.//{http://www.w3.org/2001/XInclude}include')

for i in includes:
    print(i.attrib['href'])
    _file = crud.read_data(i.attrib['href'], sid=SID).text
    name = i.attrib['href'].replace(':', '_') + '.xml'
    with open(os.path.join('export', name), 'w', encoding=ENCODING) as file:
        file.write(_file)

()
